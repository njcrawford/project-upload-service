#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 21 12:00:38 2019

@author: Martin Kopecky

    Copyright (c) 2019, Prusa Research s.r.o.


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import os
import cherrypy
import cgi
import json
import logging
import logging.config
import socket
import pyinotify
import threading
import traceback
import logging
import tempfile
import pydbus
#import gi.repository.GLib
from gi.repository.GLib import Variant as Variant

try: # deal with different variants of systemd.journal
    from systemd.journal import JournaldLogHandler
except:
    from systemd.journal import JournalHandler as JournaldLogHandler

PROFILE = False
try:
    import yappi
    yappi.start()
    PROFILE = True
except:
    pass # Suppress errors


logger = logging.getLogger()
logger.addHandler(JournaldLogHandler())
logger.setLevel(logging.DEBUG)




LOG_CONF = {
    'version': 1,

    'formatters': {
        'void': {
            'format': ''
        },
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'standard',
            'stream': 'ext://sys.stdout'
        },
        'cherrypy_console': {
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'void',
            'stream': 'ext://sys.stdout'
        },
        'cherrypy_access': {
           'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'void',
            'stream': 'ext://sys.stdout'
        },
        'cherrypy_error': {
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'void',
            'stream': 'ext://sys.stdout'
        },
    },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': 'INFO'
        },
        'db': {
            'handlers': ['default'],
            'level': 'INFO' ,
            'propagate': False
        },
    }
}


FILE_STORAGE="/var/sl1fw/projects"
SERVICE_SIGNATURE="Prusa SLA SL1 1.0.5"
API_KEY_FILE = "/etc/sl1fw/slicer-upload-api.key"
API_KEY = "asdf"
MIN_SPACE_LEFT = 110*1024*1024 # 110MB
api_key_mutex = threading.Lock() # ThreadedNotifier accesses API_KEY from different thread


def reload_api_key(path = API_KEY_FILE):
    global API_KEY
    with open(API_KEY_FILE, "r") as api_key_file:
        API_KEY_new = api_key_file.readline().replace("\r", "").replace("\n", "").strip()
    if API_KEY_new != API_KEY:
        logger.info("API key changed from \"{}\" -> \"{}\"".format(API_KEY, API_KEY_new))
        API_KEY = API_KEY_new


def will_it_fit(file_length, storage_path, file_path = None):
    #logger.info("File Size: {}".format(file_length))
    # with os.statvfs, we need to multiple block sizes by block counts to get bytes
    stats = os.statvfs(storage_path)
    disk_free = stats.f_frsize * stats.f_bavail
    logger.debug("Disk Free: {}".format(disk_free) + " File Size: {}".format(file_length))

    return file_length + MIN_SPACE_LEFT < disk_free


class StorableStorage(cgi.FieldStorage):
    """
    Custom file storage, that will handle uploaded files without making redundant
    copies.
    """
    def make_file(self, binary=None):
        #return tempfile.TemporaryFile(dir=FILE_STORAGE, prefix=".")
        return tempfile.NamedTemporaryFile(mode='w+b', buffering=0, prefix="._tmp_", dir=FILE_STORAGE)


def noBodyProcess():
    """Sets cherrypy.request.process_request_body = False, giving
    us direct control of the file upload destination. By default
    cherrypy loads it to memory, we are directing it to disk."""
    cherrypy.request.process_request_body = False


cherrypy.tools.noBodyProcess = cherrypy.Tool('before_request_body', noBodyProcess)

class Version(object):
    def __init__(self):
        pass

    @cherrypy.expose
    def index(self):
        api_key = cherrypy.request.headers.get("X-API-KEY", "")
        if api_key != API_KEY:
            cherrypy.response.status = 403
            cherrypy.response.headers["Content-Type"] = "text/plain"
            logger.info("Invalid API-KEY: \"{}\"".format(api_key))
            return "Invalid API key".encode()
        return "SL1 Octoprint-like API"

    @cherrypy.expose
    def version(self):
        api_key = cherrypy.request.headers.get("X-API-KEY", "")
        if api_key != API_KEY:
            cherrypy.response.status = 403
            cherrypy.response.headers["Content-Type"] = "text/plain"
            logger.warn("Invalid API-KEY: \"{}\"".format(api_key))
            return "Invalid API key".encode()
        cherrypy.response.headers['Content-Type'] = "application/json"
        txt = {
                    "api":"0.1",
                    "server":"1.1.0",
                    "text": SERVICE_SIGNATURE
                }
        return json.dumps(txt,ensure_ascii=True).encode()


class Files(object):
    @cherrypy.expose
    @cherrypy.tools.noBodyProcess()
    def local(self, **kw):
        #print("kw:", kw)
        cherrypy.response.timeout = 3600
        api_key_snapshot = None
        logger.info("request \"{}\"".format(cherrypy.request))
        logger.info("headers \"{}\"".format(cherrypy.request.headers))
        # Make a snapshot, easier than locking and checking for changes
        api_key_mutex.acquire_lock()
        try:
            api_key_snapshot = API_KEY
        finally:
            api_key_mutex.release()
        file_length = int(cherrypy.request.headers['Content-Length'])
        assert file_length > 0, "Uploaded file length must be greater then 0B"

        api_key = cherrypy.request.headers.get("X-API-KEY", "")
        if api_key != api_key_snapshot:
            logger.info("Invalid API key: \"{}\", should be \"{}\"".format(api_key, api_key_snapshot))
            cherrypy.response.status = 403
            cherrypy.response.headers["Content-Type"] = "text/plain"
            logger.warn("Invalid API-KEY: \"{}\"".format(api_key))
            return "Invalid API key".encode()
        if not will_it_fit(file_length, "/var"):
            cherrypy.response.status = 413
            cherrypy.response.headers["Content-Type"] = "text/plain"
            logger.error("Not enough space left on the device")
            return "Not enough space left on the device".encode()
        try:
            try:
                # Filename is still uknown at this point and parsing
                # data fields takes forever.
                o = pydbus.SystemBus().get("cz.prusa3d.sl1.NotificationSink1")
                notification_number = o.notify("uploading_file", {"severity":Variant("i", 2)})
            except:
                logger.error("Exception has occured" ,exc_info=1)
            formFields = StorableStorage(fp=cherrypy.request.rfile,
                                        headers=cherrypy.request.headers,
                                        environ={'REQUEST_METHOD':'POST'},
                                        keep_blank_values=True)
            logger.info("Receiving file: {}".format(formFields["file"].filename))
            ufile = formFields["file"]
            dst_path = os.path.join(FILE_STORAGE, ufile.filename)
            logger.debug("tmpfile: {}".format(ufile.file.name))
            try: # Remove the (possible) original file
                os.unlink(dst_path)
            except:
                pass # Fail silently
            os.link(ufile.file.name, dst_path)
            ufile.file.close()
            try:
                o.modify(notification_number, {"done":Variant("b", True),
                                               "filename":Variant("s", ufile.filename)})
            except:
                logger.error("Exception has occured" ,exc_info=1)
        except:
            logger.error("Exception has occured" ,exc_info=1)
            cherrypy.response.status = 409
            cherrypy.response.headers["Content-Type"] = "text/plain"
            logger.error("Error while writing to the file storage")
            try:
                os.unlink(ufile.file.name)
            except:
                pass # Fail silently
            return "Error while writing to the file to storage".encode()
        cherrypy.response.headers['Content-Type'] = "application/json"
        cherrypy.response.headers['Location'] = "http://{}/api/files/local/{}".format(socket.gethostname(), ufile.filename)
        response = {
                "files": {
                        "local": {
                            "name": ufile.filename,
                            "origin":"local",
                            "refs": {
                                "resource": "http://{}/api/files/local/{}".format(socket.gethostname(), ufile.filename),
                                "download": "http://{}/downloads/files/local/{}".format(socket.gethostname(), ufile.filename)
                            }
                        }
                    },
                    "done": True
                }
        if PROFILE:
            yappi.get_func_stats().print_all()
            yappi.get_thread_stats().print_all()
        return json.dumps(response, ensure_ascii=True).encode()


class EventHandler(pyinotify.ProcessEvent):
        def process_IN_CREATE(self, event):
            logger.info ("Created:", event.pathname)
            api_key_mutex.acquire_lock()
            try:
                reload_api_key()
            finally:
                api_key_mutex.release()

        def process_IN_DELETE(self, event):
            logger.info ("Removed:", event.pathname)

        def process_IN_MODIFY(self, event):
            logger.info("Changed {}".format(event))
            api_key_mutex.acquire_lock()
            try:
                reload_api_key()
            finally:
                api_key_mutex.release()


if __name__ == "__main__":
    cherrypy.config.update({'log.screen': True,
                                #'log.access_file': '/tmp/cherry_access.log',
                                #'log.error_file': '/tmp/cherry_err.log'
                                'server.socket_host': '127.0.0.1',
                                'server.socket_port': 7999,
                                'server.max_request_body_size' : 0,
                                'server.socket_timeout': 60,
                                'server.thread_pool': 1,
                                'response.timeout': 3600,
                                })
    cherrypy.engine.unsubscribe('graceful', cherrypy.log.reopen_files)

    # Remove temporary files if there are any(there should not be)
    r = os.system("rm {}/._tmp_*".format(FILE_STORAGE))
    if r == 0:
        logger.warning("Temporary files in {} deleted".format(FILE_STORAGE))
    # The FILE_STORAGE must exist
    os.makedirs(FILE_STORAGE, exist_ok=True, mode=777)

    reload_api_key(API_KEY_FILE)

    cherrypy.tree.mount(Version(), "/api")
    cherrypy.tree.mount(Files(), "/api/files")

    # The watch manager stores the watches and provides operations on watches
    wm = pyinotify.WatchManager()
    mask = pyinotify.IN_DELETE | pyinotify.IN_CREATE | pyinotify.IN_MODIFY # watched events

    notifier = pyinotify.ThreadedNotifier(wm, EventHandler())

    # Start the notifier from a new thread, without doing anything as no directory or file are currently monitored yet.
    notifier.start()

    # Start watching a path
    wdd = wm.add_watch(API_KEY_FILE, mask)


    cherrypy.engine.start()
    cherrypy.engine.block()

    notifier.stop()

    if PROFILE:
        yappi.get_func_stats().print_all()
        yappi.get_thread_stats().print_all()

